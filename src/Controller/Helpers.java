
package Controller;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author anhnb
 */
public class Helpers {
    public String formatDate(String s) throws Exception{
        try {
        SimpleDateFormat formatter1 ;
        formatter1 = new SimpleDateFormat("dd/MM/yyyy");
        Date date = formatter1.parse(s);
        formatter1 = new SimpleDateFormat("dd-MMM-yyyy");
        String temp = formatter1.format(date);
        return temp;
        } catch (Exception e) {
            throw e;
        }
       
    }
}
