
package Controller;

import Model.Expense;
import java.util.ArrayList;
import java.util.Scanner;


public class Management {
    ArrayList<Expense> list;
    Scanner in;
    int id = 0;
    Helpers helpers;
    public Management() {
        list = new ArrayList<>();
        in = new Scanner(System.in);
        helpers = new Helpers();
    }
    
    public void test(){
        list.add(new Expense(1, "21-8-2020", 65, "content1"));
        list.add(new Expense(2, "21-8-2020", 96, "content1"));
        list.add(new Expense(3, "21-8-2020", 65, "content1"));
    }
    public boolean add(){
        Expense expense = new Expense();
        try {
        id+=1;
        expense.setId(id);
        System.out.println("Enter date: ");// 21/08/2020 =>21/Aug/2020
        String date;
        date = in.nextLine();
        expense.setDate( helpers.formatDate(date));
        
        System.out.println("Enter number: ");
        double amount;
        amount = in.nextDouble();
        expense.setNumber(amount);
        System.out.println("Enter content: ");
        String content;
        in.nextLine();
        content = in.nextLine();
        expense.setContent(content);
        return list.add(expense);
        
        } catch (Exception e) {
            System.out.println("error "+e);
        }
       
        return false;
    }
     protected void display(){
       // System.out.println( list.toString());
         System.out.printf("%-5s%-10s%10s%10s\n", "ID", "Date", "Amount","Content");
        for (Expense expense : list) {
            System.out.printf("%-5s%-10s%-10s%-10s\n", expense.getId(), 
                    expense.getDate(), expense.getNumber(),expense.getContent());
        }
    }
     
    public boolean isDelete(){
        //enter id
        //=> find id => exits or not => delete
        try {
            System.out.println("Enter id: ");
            int id =in.nextInt();
            for (Expense expense : list) {
                if(expense.getId()== id){
                    return list.remove(expense);
                }
            }
        } catch (Exception e) {
            System.err.println("Err: "+e);
        }
        return false;
    }
    
}
